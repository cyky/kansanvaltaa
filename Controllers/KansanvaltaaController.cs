using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Kansanvaltaa.Models;
using Kansanvaltaa.Repository;
using Kansanvaltaa.Controllers.Avoindata;
using Microsoft.Extensions.Logging;

namespace Kansanvaltaa.Controllers
{
    [Route("api/[controller]")]
    public class KansanvaltaaController : Controller
    {
        protected static KansanvaltaaRepository Repository;
        private readonly ILogger<KansanvaltaaController> _logger;
        public KansanvaltaaController(ILogger<KansanvaltaaController> logger)
        {
            _logger = logger;
        }

        [HttpGet("[action]")]
        public List<Question> AvoindataQuestions()
        {
            Repository = new KansanvaltaaRepository();
            var avoindataQuestions = AvoindataHandler.GetAllQuestions();
            Repository.InsertQuestions(avoindataQuestions.Result);
            List<Question> result = Repository.GetAllQuestions();
            return result.OrderByDescending(x => x.created).ToList();
        }

        [HttpGet("[action]")]
        public Question GetQuestion(string id)
        {
            Question result = new Question();
            AvoindataQuestionResponseModel resultFromApi = AvoindataHandler.GetQuestion(id).Result;
            if (resultFromApi != null && resultFromApi.question.First() != null)
            {
                Question questionFromApi = resultFromApi.question[0];
                questionFromApi.answers = resultFromApi.answers;
                result = Repository.ReplaceQuestion(questionFromApi);
            }
            else
            {
                result = Repository.Get(id);
            }
            List<Answer> localAnswers = Repository.GetLocalAnswersByQuestionId(result.postid);
            if (localAnswers.Count > 0)
            {
                if (result.answers != null)
                {
                    result.answers.AddRange(localAnswers);
                }
                else
                {
                    result.answers = localAnswers;
                }

            }
            return result;
        }

        [HttpPost("[action]")]
        public Answer PostAnswer([FromBody] Answer newAnswer)
        {
            if (newAnswer != null)
            {
                return Repository.InsertNewLocalAnswer(newAnswer);
            }
            return null;
        }
    }
}
