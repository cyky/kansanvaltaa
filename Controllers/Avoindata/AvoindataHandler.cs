using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kansanvaltaa.Models;

namespace Kansanvaltaa.Controllers.Avoindata
{
    public class AvoindataHandler
    {

        private static string baseUrl = "http://api.avoindata.net/";
        private static string questionsPath = "questions";
        private static string tagsPath = "tags";
        private static string categoriesPath = "categories";
        public static async Task<List<Question>> GetRecentQuestions()
        {
            var response = AvoindataGet(questionsPath).Result;
            if (response.IsSuccessStatusCode)
            {
                AvoindataQuestionsResponseModel data = Newtonsoft.Json.JsonConvert.DeserializeObject<AvoindataQuestionsResponseModel>(await response.Content.ReadAsStringAsync());
                return data.questions;
            }
            return new List<Question>();
        }

        public static async Task<List<Category>> GetCategories()
        {

            var response = AvoindataGet(categoriesPath).Result;
            if (response.IsSuccessStatusCode)
            {
                AvoindataCategoriesResponseModel data = Newtonsoft.Json.JsonConvert.DeserializeObject<AvoindataCategoriesResponseModel>(await response.Content.ReadAsStringAsync());
                return data.categories;
            }
            return new List<Category>();
        }

        public static async Task<List<Question>> GetAllQuestions()
        {
            List<Category> allCategories = GetCategories().Result;
            List<Question> resultQuestions = new List<Question>();

            foreach (Category cat in allCategories)
            {
                var responseMessage = AvoindataGet($"{categoriesPath}/id/{cat.catid}").Result;
                if (responseMessage.IsSuccessStatusCode)
                {
                    AvoindataQuestionsResponseModel data = Newtonsoft.Json.JsonConvert.DeserializeObject<AvoindataQuestionsResponseModel>(await responseMessage.Content.ReadAsStringAsync());
                    if (data.questions.Count > 0)
                    {
                        resultQuestions.AddRange(data.questions);
                    }
                }
            }
            return resultQuestions;
        }

        public static async Task<AvoindataQuestionResponseModel> GetQuestion(string id)
        {
            var response = AvoindataGet($"{questionsPath}/id/{id}").Result;
            if (response.IsSuccessStatusCode)
            {
                try
                {
                    AvoindataQuestionResponseModel data = Newtonsoft.Json.JsonConvert.DeserializeObject<AvoindataQuestionResponseModel>(await response.Content.ReadAsStringAsync());
                    return data;
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error while deserializing JSON data for Question id {id}. Error: {e}");
                }
            }
            return null;
        }

        private static async Task<HttpResponseMessage> AvoindataGet(string path)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                httpClient.BaseAddress = new Uri(baseUrl);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                return await httpClient.GetAsync(path);
            }
        }
    }
}