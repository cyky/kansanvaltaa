using MongoDB.Bson;
using System.Collections.Generic;
using System;

namespace Kansanvaltaa.Models
{
    public class Question
    {
        public string title { get; set; }
        public int id { get; set; }
        public int postid
        {
            get => id;
            set => id = value;
        }
        public object userid { get; set; }
        public string userhandle { get; set; }
        public string usergravatar { get; set; }
        public string content { get; set; }
        public int viewcount { get; set; }
        public int viewscount
        {
            get => viewcount;
            set => viewcount = value;
        }
        public int votes { get; set; }
        public int netvotes
        {
            get => votes;
            set => votes = value;
        }
        public int created { get; set; }
        public int updated { get; set; }
        public int answercount { get; set; }
        public List<Answer> answers { get; set; }
        public string url { get; set; }
        public List<string> tags { get; set; }
    }
    public class Answer
    {
        public string postid { get; set; }

        public int id
        {
            get => Convert.ToInt32(postid);
            set => postid = value.ToString();
        }
        public string questionid { get; set; }
        public string votes { get; set; }
        public string content { get; set; }
        public int created { get; set; }
    }

    public class Category
    {
        public string title { get; set; }
        public int count { get; set; }
        public int catid { get; set; }
    }
    public class AvoindataRights
    {
        public string contentLicense { get; set; }
        public string dataLicense { get; set; }
        public string copyrightNotice { get; set; }
        public string attributionText { get; set; }
        public string attributionURL { get; set; }
    }
    public class AvoindataQuestionsResponseModel
    {
        public List<Question> questions { get; set; }
        public List<AvoindataRights> rights { get; set; }
    }

    public class AvoindataQuestionResponseModel
    {
        public List<Question> question { get; set; }
        public List<Answer> answers { get; set; }
        public List<AvoindataRights> rights { get; set; }
    }

    public class AvoindataCategoriesResponseModel
    {
        public List<Category> categories { get; set; }
        public List<AvoindataRights> rights { get; set; }
    }

}