import Vue from 'vue';
import { Component, Prop } from 'vue-property-decorator';
import axios from 'axios'

interface Question {
    title: string;
    id: string;
    userid: string;
    userhandle: string;
    usergravatar: string;
    content: string;
    viewcount: number;
    votes: number;
    created: number;
    updated: number;
    answercount: number;
    answers: Answer[];
    url: string;
    tags: string[];
}

interface Answer {
    postid: string;
    questionid: string;
    votes: string;
    content: string
    created: number;
}

@Component
export default class QuestionDataComponent extends Vue {
    questions: Question[] = [];
    selectedTags: string = '';
    searchFilter: string = '';
    selectedQuestion: number = null;
    userAnswer: string = '';
    showAnswerField: boolean = true;

    getQuestionInfo(id) {
        if (this.selectedQuestion === id) { return; }
        this.selectedQuestion = id;
        this.userAnswer = "";
        this.showAnswerField = true;
        fetch('/api/Kansanvaltaa/GetQuestion?id=' + id)
            .then(response => response.json() as Promise<Question>)
            .then(data => {
                this.questions.forEach((item, index) => {
                    if (item.id === id) {
                        this.questions.splice(index, 1, data)
                    }
                })
            })
            .catch((err) => {
                console.log('Error getting question infromation from Avoindata.net with id ', id, ' error:', err);
            })
    }

    postQuestionAnswer(id) {
        this.showAnswerField = false;
        if (this.userAnswer.length === 0) { return; }
        var postUrl = 'http://localhost:5000/api/Kansanvaltaa/PostAnswer';
        axios.post('/api/Kansanvaltaa/PostAnswer', {
            'postid': '1',
            'questionid': id.toString(),
            'votes': '0',
            'content': this.userAnswer.toString(),
            'created': Math.floor((new Date).getTime() / 1000)
        }, { headers: { 'Content-Type': 'application/json' } })
            .then(response => {
                if (response.statusText == 'OK') {
                    console.log(JSON.stringify(response.data))
                    this.questions.forEach((item, index) => {
                        if (item.id === id) {
                            let newAnswer: Answer = {
                                postid: response.data.postid,
                                questionid: response.data.questionid,
                                votes: response.data.votes,
                                content: response.data.content,
                                created: response.data.created
                            }
                            if(item.answers != null) {
                                item.answers.push(newAnswer);
                            } else {
                                item.answers = [newAnswer];
                            }
                            this.questions.splice(index, 1, item);
                        }
                    })
                }
            })
            .catch(error => {
                console.log('Error submitting user answer.')
            });
        this.userAnswer = "";
    }
    
    get questionTagFilter() {
        return (this.searchFilter.length > 0 || this.selectedTags.length > 0) ?
            this.questions.filter((question) => {
                return (this.searchFilter.length === 0 ? true : question.title.toLocaleLowerCase().indexOf(this.searchFilter.toLocaleLowerCase()) >= 0)
                    && question.tags.some((element) => {
                        return this.selectedTags.length === 0 ? true : this.selectedTags.toLocaleLowerCase().indexOf(element.toLocaleLowerCase()) >= 0;;
                    })
            }) : this.questions;
    }

    mounted() {
        fetch('/api/Kansanvaltaa/AvoindataQuestions')
            .then(response => response.json() as Promise<Question[]>)
            .then(data => {
                this.questions = data;
            });
    }
}