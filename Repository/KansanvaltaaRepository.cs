using System;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using Kansanvaltaa.Models;
using MongoDB.Bson.Serialization;

namespace Kansanvaltaa.Repository
{
    public class KansanvaltaaRepository
    {
        protected static IMongoClient Client;
        protected static IMongoDatabase Database;
        protected IMongoCollection<AvoindataQuestionResponseModel> Collection;
        protected IMongoCollection<Question> Question;
        protected IMongoCollection<Answer> LocalAnswers;

        public KansanvaltaaRepository()
        {
            Client = new MongoClient("mongodb://localhost:27017");
            Database = Client.GetDatabase("kansanvaltaa");
            Question = Database.GetCollection<Question>("question");
            LocalAnswers = Database.GetCollection<Answer>("answers");
        }

        public Question InsertQuestion(Question question)
        {
            Question.InsertOneAsync(question);
            return Get(question.id.ToString());
        }

        public List<Question> InsertQuestions(List<Question> q)
        {
            var distinctQuestions = q.GroupBy(x => x.id).Select(g => g.First()).ToList();
            foreach (Question item in distinctQuestions)
            {
                UpdateQuestion(item);
            }
            List<int> ids = q.Select(x => x.id).ToList();
            return Question.Find<Question>(x => ids.Contains(x.id)).ToListAsync().Result;
        }

        public void UpdateQuestion(Question q)
        {
            var inDb = Get(q.id.ToString());
            if (inDb == null)
            {
                InsertQuestion(q);
            }
            else
            {
                var filter = Builders<Question>.Filter.Eq("_id", q.id);
                var update = Builders<Question>.Update.Set("title", q.title).AddToSet("url", q.url).AddToSet("viewcount", q.viewcount).AddToSet("answerscount", q.answercount).AddToSet("votes", q.netvotes).AddToSet("tags", q.tags);
                var updateOptions = new UpdateOptions() { IsUpsert = true };
                Question.UpdateOneAsync(filter, update, updateOptions);
            }
        }

        public List<Question> GetAllQuestions()
        {
            return Question.Find(new BsonDocument()).ToListAsync().Result;
        }

        public Question Get(string id)
        {
            return Question.Find(x => x.id.Equals(id)).FirstOrDefault();
        }

        public Question ReplaceQuestion(Question question)
        {
            Question.ReplaceOneAsync(q => q.id.Equals(question.id), question);
            return Get(question.id.ToString());
        }

        public Answer InsertNewLocalAnswer(Answer a)
        {
            Answer max = LocalAnswers.Find(new BsonDocument()).ToList().OrderByDescending(x => x.created).FirstOrDefault();
            Int32 maxPostId = 0;
            if (max != null)
            {
                Int32.TryParse(max.postid, out maxPostId);
            }
            a.postid = (maxPostId + 1).ToString();
            LocalAnswers.InsertOneAsync(a);
            return LocalAnswers.Find(x => x.postid == a.postid).FirstOrDefault();
        }

        public List<Answer> GetLocalAnswersByQuestionId(int questionid){
            return LocalAnswers.Find(x => x.questionid.Equals(questionid)).ToList();
        }
    }
}
